import { Injectable } from '@nestjs/common';
import OpenAI from 'openai';
import * as fs from 'fs';

// const model = 'gpt-3.5-turbo-0301';
const model = 'gpt-4-vision-preview';
const textModel = 'tts-1';
const speechModel = 'whisper-1';

@Injectable()
export class AppService {
  api = new OpenAI({
    apiKey: process.env.OPENAI_API_KEY,
  });

  getHello(): string {
    return 'Hello World!';
  }

  async getChatCompletion(messages: any): Promise<any> {
    console.log('Completion api called at ', new Date());
    try {
      const completion = await this.api.chat.completions.create({
        messages: [...messages],
        model: model,
        ...(model === 'gpt-4-vision-preview' && {
          max_tokens: 4096,
        }),
      });
      const message = completion.choices[0].message;
      return {
        id: completion.id,
        ...message,
        text: message.content,
        createdAt: new Date(),
      };
    } catch (error) {
      console.log('Error getting response from OpenAI', error);
    }
  }

  async getTextToSpeech(input: any): Promise<any> {
    console.log('TTS api called at ', new Date());
    try {
      const response = await this.api.audio.speech.create({
        model: textModel,
        voice: 'alloy',
        input: input.text,
      });
      const mp3Buffer = Buffer.from(await response.arrayBuffer());
      const filePath = `./audio/sound.mp3`;
      await fs.promises.writeFile(filePath, mp3Buffer);

      const contents = await fs.promises.readFile(filePath, {
        encoding: 'base64',
      });
      return {
        filePath,
        contents,
      };
    } catch (error) {
      console.log('Error getting response from OpenAI', error);
    }
  }

  async getSpeechToText(input: any): Promise<any> {
    console.log('STT api called at ', new Date());
    try {
      const base64 = input.audio;
      const filePath = `./speech/recording.mp4`;
      await fs.promises.writeFile(filePath, base64, {
        encoding: 'base64',
      });
      const response = await this.api.audio.transcriptions.create({
        model: speechModel,
        file: fs.createReadStream(filePath),
      });
      return response;
    } catch (error) {
      console.log('Error getting response from OpenAI', error);
    }
  }
}
