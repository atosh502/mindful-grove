import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('/chat')
  response(@Body() messages: any): any {
    return this.appService.getChatCompletion(messages);
  }

  @Post('/tts')
  textToSpeech(@Body() textInput: any): any {
    return this.appService.getTextToSpeech(textInput);
  }

  @Post('/stt')
  speechToText(@Body() audioInput: any): any {
    return this.appService.getSpeechToText(audioInput);
  }
}
