import uuid from 'react-native-uuid';

export const llmBot = {
  _id: uuid.v4(),
  name: 'Mindful Grove',
  avatar: require('./assets/images/app_icon.png'),
};

export const initMessages = [
  {
    _id: uuid.v4(),
    text: 'You are an empathetic assistant supporting people suffering from mental illness. Please reply in two sentences or less.',
    role: 'system',
    hidden: true,
    createdAt: `${new Date()}`,
    user: llmBot,
  },
  {
    _id: uuid.v4(),
    text: "I'm here to listen and support you. You are not alone in this and together we will work towards finding the best possible outcome for you.",
    role: 'assistant',
    createdAt: `${new Date()}`,
    user: llmBot,
  },
];
