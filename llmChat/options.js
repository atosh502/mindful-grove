export const imagePickerOptions = {
  selectionLimit: 1,
  mediaType: 'photo',
  includeBase64: true,
  quality: 0.1,
};

export const imageCaptureOptions = {
  saveToPhotos: true,
  mediaType: 'photo',
  includeBase64: true,
  quality: 0.1,
};
