import RNFS from 'react-native-fs';

export const deleteFileIfExists = async fileName => {
  const fileExists = await RNFS.exists(fileName);
  if (fileExists) {
    await RNFS.unlink(fileName);
  }
};
