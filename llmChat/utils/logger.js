import analytics from '@react-native-firebase/analytics';

export const logEvent = async messages => {
  const message = messages[0];
  const key = message.inputSrc ?? 'text';
  const {imageUri, imageBase64, ...context} = message;
  const {_id: id, user, ...rest} = context;
  await analytics().logEvent(`${key}_input`, {
    ...rest,
    id,
    userId: user?._id,
  });
};
