import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

export const MicroPhone = () => (
  <Icon name="microphone" size={20} color="#0084ff" />
);
export const Camera = () => <Icon name="camera" size={20} color="#0084ff" />;

export const Play = () => <Icon name="play-circle" size={25} color="#0084ff" />;

export const Pause = () => (
  <Icon name="pause-circle" size={25} color="#0084ff" />
);

export const Stop = () => <Icon name="stop" size={20} color="#0084ff" />;
