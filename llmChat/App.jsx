/* eslint-disable no-shadow */
import React, {useCallback, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Button,
  PermissionsAndroid,
  Platform,
  SafeAreaView,
  StatusBar,
  Text,
  View,
  useColorScheme,
} from 'react-native';

import AudioRecorderPlayer from 'react-native-audio-recorder-player';
import RNFS from 'react-native-fs';
import {GiftedChat} from 'react-native-gifted-chat';
import Sound from 'react-native-sound';
import uuid from 'react-native-uuid';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {getCompletion, getSpeechToText, getTextToSpeech} from './api';
import {
  renderCustomFooter,
  renderViewWithAudioPlayback,
} from './components/CustomChatComponents';
import {
  renderCustomActions,
  renderCustomInputToolbar,
} from './components/CustomToolbar';
import {initMessages, llmBot} from './constants';
import {deleteFileIfExists} from './utils';
import {logEvent} from './utils/logger';
import auth from '@react-native-firebase/auth';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import {onGoogleButtonPress} from './utils/auth';
import {GoogleSigninButton} from '@react-native-google-signin/google-signin';

const audioRecorderPlayer = new AudioRecorderPlayer();

GoogleSignin.configure({
  webClientId:
    '47894929881-8c800l73552o9jr77nb45v7n549kic7l.apps.googleusercontent.com',
});

function App() {
  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    flex: 1,
  };

  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const userId = user?.uid;

  function onAuthStateChanged(user) {
    console.log('🚀 ~ file: App.jsx:57 ~ onAuthStateChanged ~ user:', user);
    setUser(user);
    if (initializing) setInitializing(false);
  }

  const [image, setImage] = useState(null);

  const handleResponse = response => {
    setImage(response);
  };

  const [isRecording, setIsRecording] = useState(false);

  const [messages, setMessages] = useState([...initMessages]);
  const [isThinking, setIsThinking] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const [isProcessingAudio, setIsProcessingAudio] = useState(false);
  const [currPlayback, setCurrPlayback] = useState(null);
  const [currMessageId, setCurrentMessageId] = useState(null);
  const [currFilePath, setCurrentFilePath] = useState(null);
  const [isTranscribing, setIsTranscribing] = useState(false);

  const [recordState, setRecordState] = useState({});

  const onStartRecord = useCallback(async () => {
    if (Platform.OS === 'android') {
      try {
        const grants = await PermissionsAndroid.requestMultiple([
          PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        ]);

        console.log('granted permissions', grants);

        if (
          grants['android.permission.RECORD_AUDIO'] ===
          PermissionsAndroid.RESULTS.GRANTED
        ) {
          setIsRecording(true);
          const fileName = RNFS.ExternalDirectoryPath + '/recording.mp4';
          await deleteFileIfExists(fileName);

          await audioRecorderPlayer.startRecorder(fileName);
          audioRecorderPlayer.addRecordBackListener(e => {
            setRecordState({
              recordSecs: e.currentPosition,
              recordTime: audioRecorderPlayer.mmssss(
                Math.floor(e.currentPosition),
              ),
            });
            return;
          });
        } else {
          console.log('All required permissions not granted');
          return;
        }
      } catch (err) {
        console.warn(err);
        return;
      }
    }
  }, []);

  const onStopRecord = useCallback(async () => {
    const result = await audioRecorderPlayer.stopRecorder();
    setIsTranscribing(true);
    audioRecorderPlayer.removeRecordBackListener();
    setRecordState({
      recordSecs: 0,
    });

    const response = await RNFS.readFile(result, 'base64');
    try {
      const res = await getSpeechToText({
        audio: response,
      });
      const {text} = res;
      onSend([
        {
          _id: uuid.v4(),
          createdAt: `${new Date()}`,
          user: {_id: userId},
          text: text,
          inputSrc: 'audio',
        },
      ]);
    } catch (error) {
      console.log('Error transcribing audio', error);
    } finally {
      setIsTranscribing(false);
      setIsRecording(false);
    }
  }, [onSend, userId]);

  const getResponse = async messages => {
    const chats = messages.map(m => {
      if (!m.imageBase64) {
        return {
          role: m.role,
          content: m.text,
        };
      } else {
        return {
          role: m.role,
          content: [
            {type: 'text', text: m.text},
            {
              type: 'image_url',
              image_url: {
                url: `data:image/jpg;base64,${m.imageBase64}`,
              },
            },
          ],
        };
      }
    });
    return await getCompletion(chats);
  };

  const playResponse = async (text, id) => {
    try {
      setCurrentMessageId(id);
      setIsProcessingAudio(true);
      const audioResponse = await getTextToSpeech({
        text,
      });

      const filePath = RNFS.ExternalDirectoryPath + '/audio.mp3';
      setCurrentFilePath(filePath);
      await deleteFileIfExists(filePath);

      await RNFS.writeFile(filePath, audioResponse.contents, 'base64');

      setIsProcessingAudio(false);
      let playback = new Sound(filePath, Sound.MAIN_BUNDLE, error => {
        if (error) {
          console.log('failed to load the sound', error);
          return;
        }

        setCurrPlayback(playback);
        playback.play(success => {
          playback.release();
          setIsPlaying(false);
          setCurrPlayback(null);

          if (success) {
            console.log('successfully finished playing');
          } else {
            console.log('playback failed due to audio decoding errors');
          }
        });
      });

      setCurrentFilePath(filePath);
    } catch (error) {
      console.log('Error while saving the file', error);
    }
  };

  const onSend = useCallback(
    async (newMessages = []) => {
      const imageUri = image?.assets[0]?.uri;
      const imageBase64 = image?.assets[0]?.base64;
      // clear the image
      setImage(null);

      // display image into gifted chat's ui
      const messagesWithRole = newMessages.map(m => ({
        role: 'user',
        ...(imageUri && {image: imageUri, inputSrc: 'image'}),
        ...(imageBase64 && {imageBase64: imageBase64}),
        ...m,
      }));

      await logEvent(messagesWithRole);

      const allMessages = [...messages, ...messagesWithRole];
      setMessages(previousMessages =>
        GiftedChat.append(previousMessages, messagesWithRole),
      );

      setIsThinking(true);
      let response = await getResponse(allMessages);

      setMessages(previousMessages =>
        GiftedChat.append(previousMessages, [
          {
            ...response,
            _id: response.id,
            image: imageUri,
            user: llmBot,
          },
        ]),
      );
      setIsThinking(false);
    },
    [image?.assets, messages],
  );

  const renderInputToolbar = useCallback(props => {
    return renderCustomInputToolbar({
      ...props,
    });
  }, []);

  const renderCustomView = useCallback(
    props => {
      return renderViewWithAudioPlayback({
        ...props,
        currMessageId,
        isPlaying,
        currPlayback,
        isProcessingAudio,
        setIsPlaying,
        playResponse,
      });
    },
    [currMessageId, isPlaying, currPlayback, isProcessingAudio],
  );

  const renderChatFooter = useCallback(
    props => {
      return renderCustomFooter({
        ...props,
        isTranscribing,
        isRecording,
        image,
      });
    },
    [isTranscribing, isRecording, image],
  );

  const renderActions = useCallback(
    props => {
      return renderCustomActions({
        ...props,
        isRecording,
        isTranscribing,
        handleResponse,
        onStartRecord,
        onStopRecord,
      });
    },
    [isRecording, isTranscribing],
  );

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator color={'#0084ff'} size={'large'} />
      </View>
    );
  }

  if (!user) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <GoogleSigninButton
          onPress={() =>
            onGoogleButtonPress().then(() =>
              console.log('Signed in with Google!'),
            )
          }
          size={GoogleSigninButton.Size.Wide}
        />
      </View>
    );
  }

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <View style={{flex: 1}}>
        <GiftedChat
          messages={messages.filter(m => !m.hidden)}
          onSend={messages => onSend(messages)}
          renderInputToolbar={renderInputToolbar}
          user={{
            _id: userId,
          }}
          renderActions={renderActions}
          renderChatFooter={renderChatFooter}
          isTyping={isThinking}
          isCustomViewBottom={true}
          renderCustomView={renderCustomView}
        />
      </View>
    </SafeAreaView>
  );
}

export default App;
