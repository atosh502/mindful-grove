import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://localhost:3000',
  timeout: 5000,
});

export const getCompletion = async messages => {
  try {
    const response = await instance.post('/chat', messages);
    return response.data;
  } catch (error) {
    console.log('axios error', error);
  }
};

export const getTextToSpeech = async messages => {
  try {
    const response = await instance.post('/tts', messages);
    return response.data;
  } catch (error) {
    console.log('axios error', error);
  }
};

export const getSpeechToText = async audioInput => {
  try {
    const response = await instance.post('/stt', audioInput);
    return response.data;
  } catch (error) {
    console.log('axios error', error);
  }
};
