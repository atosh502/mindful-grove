import React from 'react';
import {useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

export const TickingText = ({label}) => {
  const [tickCount, setTickCount] = useState(1);

  useEffect(() => {
    const interval = setInterval(() => {
      setTickCount(tickCount => (tickCount + 1) % 4);
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: '2%',
        marginHorizontal: '4%',
        marginVertical: '2%',
        borderRadius: 10,
        color: Colors.darker,
      }}>
      <Text>{`${label}${'.'.repeat(tickCount)}`}</Text>
    </View>
  );
};
