import {Image, View} from 'react-native';
import {Actions, Composer, InputToolbar} from 'react-native-gifted-chat';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {Camera, MicroPhone, Pause, Stop} from '../utils/icons';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {imageCaptureOptions, imagePickerOptions} from '../options';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useEffect, useState} from 'react';

export const renderCustomInputToolbar = props => {
  const {image} = props;
  return (
    <>
      <InputToolbar
        {...props}
        renderComposer={cProps => (
          <Composer
            {...cProps}
            textInputStyle={{
              color: Colors.darker,
            }}
          />
        )}
      />
    </>
  );
};

export const renderCustomActions = props => {
  const {
    handleResponse,
    isRecording,
    isTranscribing,
    onStopRecord,
    onStartRecord,
  } = props;

  return (
    <>
      {isRecording && (
        <Actions
          {...props}
          icon={() => <Icon name={'pause'} size={20} color="#0084ff" />}
          containerStyle={{
            justifyContent: 'center',
            alignItems: 'center',
          }}
          di
          onPressActionButton={() => {
            if (isTranscribing) {
              return;
            }
            if (isRecording) {
              onStopRecord();
            } else {
              onStartRecord();
            }
          }}
        />
      )}
      {!isRecording && (
        <Actions
          {...props}
          icon={() => <Icon name={'microphone'} size={20} color="#0084ff" />}
          containerStyle={{
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPressActionButton={() => {
            if (isTranscribing) {
              return;
            }
            if (isRecording) {
              onStopRecord();
            } else {
              onStartRecord();
            }
          }}
        />
      )}
      <Actions
        {...props}
        icon={Camera}
        containerStyle={{
          justifyContent: 'center',
          alignItems: 'center',
        }}
        options={{
          'Choose From Library': () => {
            launchImageLibrary(imagePickerOptions, handleResponse);
          },
          'Open Camera': () => {
            launchCamera(imageCaptureOptions, handleResponse);
          },
          Cancel: () => {
            console.log('Cancel');
          },
        }}
        optionTintColor="#222B45"
      />
    </>
  );
};
