import React from 'react';
import {TickingText} from './TickingText';
import {Image, Pressable, View} from 'react-native';
import analytics from '@react-native-firebase/analytics';
import {TextLink} from './TextLink';

export const renderCustomFooter = ({isTranscribing, isRecording, image}) => {
  return (
    <View
      style={{
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: '2%',
      }}>
      {image && image?.assets && (
        <Image
          style={{
            width: 64,
            height: 64,
            borderRadius: 8,
          }}
          source={{
            uri: image.assets[0]?.uri,
          }}
        />
      )}
      {isTranscribing ? (
        <TickingText label="Processing audio" />
      ) : isRecording ? (
        <TickingText label="Recording your voice now" />
      ) : null}
    </View>
  );
};

export const renderViewWithAudioPlayback = props => {
  const {
    currMessageId,
    isPlaying,
    currPlayback,
    setIsPlaying,
    playResponse,
    isProcessingAudio,
  } = props;
  const id = props.currentMessage._id;
  const isCurrentMessage = id === currMessageId;
  return (
    <View
      style={{
        alignItems: 'center',
      }}>
      {props?.currentMessage?.role === 'assistant' ? (
        <Pressable
          onPress={async () => {
            const text = props.currentMessage.text;
            if (text) {
              const {_id: id, ...rest} = props.currentMessage;
              console.log(
                '🚀 ~ file: CustomChatComponents.jsx:59 ~ onPress={ ~ rest:',
                rest,
              );
              await analytics().logEvent('audio_output', {
                ...rest,
                id,
              });
            }
            if (isCurrentMessage) {
              if (isPlaying && currPlayback) {
                setIsPlaying(false);
                currPlayback.stop();
              } else {
                setIsPlaying(true);
                await playResponse(text, id);
              }
            } else {
              setIsPlaying(true);
              await playResponse(text, id);
            }
          }}
          disabled={isProcessingAudio}
          style={{
            marginHorizontal: '4%',
            flexDirection: 'row',
            justifyContent: 'flex-end',
          }}>
          {isCurrentMessage ? (
            isProcessingAudio ? (
              <TextLink label="Processing..." />
            ) : isPlaying ? (
              <TextLink label="Stop" />
            ) : (
              <TextLink label="Play" />
            )
          ) : (
            <TextLink label="Play" />
          )}
        </Pressable>
      ) : null}
    </View>
  );
};
