import {Text} from 'react-native';

export const TextLink = ({label}) => {
  return (
    <Text
      style={{
        color: '#0084ff',
      }}>
      {label}
    </Text>
  );
};
