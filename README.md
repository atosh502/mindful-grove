## ui-ux-design-fall2023

### chat-server

- Install dependencies with `npm install`
- Add OpenAI API key(`OPENAI_API_KEY`) to `.env` file
- Start the server with `npm run start:dev`

### chatbot

- Install dependencies with `npm install`
- If running locally on a physical device. Find the device id with `adb devices`. Then run `adb -s <device_id> reverse tcp:3000 tcp:3000` (server runs on port 3000) in order for app to access the local server.
- Start the server with `npm start`. Then press `a` to run on Android.
